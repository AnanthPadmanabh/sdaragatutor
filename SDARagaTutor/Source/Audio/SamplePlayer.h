/*
 ==================================================================================
 
 SamplePlayer.h
 Created: 28 Jan 2020 11:56:40pm
 Author:  Anantha Girijavallabhan
 
 ==================================================================================
 */
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
//==================================================================================
/**
 
    Base class for the sample player.
 
    This class is based roughly on the JUCE Tutorial: Build an audio player.
 
    @see DroneSamplePlayer
    @see TablaSamplePlayer
 */
class SamplePlayer  : public ChangeListener
{
public:
    /** Constructor */
    SamplePlayer ();
    
    /** Destructor */
    ~SamplePlayer ();
    //==============================================================================
    
    /** Loads the appropriate sample file for the currently selected key.
     
        @param key          an index representing the sampler file to be used for the key
     
        @see setUpSampler
    */
    void loadSampleFile (String file);
    
    /** Streams the sampler to the output.
     
        @see loadSampleFile
    */
    void setUpSampler ();
    
    /** Returns the audio transport source */
    AudioTransportSource& getTransport () { return transportSource; }
    
    /** Returns whether or not the sample is currently playing.
     
        @return current play state of the sample
    */
    bool isPlaying () { return transportSource.isPlaying(); }
    
    /** Sets the transport state to stop.
     
        @see setStarting
    */
    void setStopping ();
    
    /** Sets the transport state to start.
     
        @see setStopping
    */
    void setStarting ();
    
    /** Sets the gain of the sample.
     
        @param gain         the float gain of the sample where 1.0 is maximum
    */
    void setGain(float gain);
    
    //Overriden=====================================================================
    /** Listens to changes in the audio transport state.
     */
    void changeListenerCallback (ChangeBroadcaster* source) override;
    
private:
    //==============================================================================
    enum TransportState
    {
        Stopped,
        Starting,
        Playing,
        Stopping
    };
    
    /** Changes the current transport state.
     
     @param newState     an enum representing the current transport state
     
     @see setStopping
     */
    void changeState (TransportState newState)
    {
        if (state != newState)
        {
            state = newState;
            
            switch (state)
            {
                case Stopped:
                    transportSource.setPosition (0.0);
                    transportSource.start();
                    break;
                    
                case Starting:
                    transportSource.start();
                    break;
                    
                case Playing:
                    
                    break;
                    
                case Stopping:
                    transportSource.stop();
                    break;
            }
        }
    }
    //==============================================================================
    
    TransportState state;
    AudioFormatManager formatManager;
    std::unique_ptr<AudioFormatReaderSource> readerSource;
    AudioTransportSource transportSource;
    AudioSampleBuffer fileBuffer;
    
    String fileName;
    
};
