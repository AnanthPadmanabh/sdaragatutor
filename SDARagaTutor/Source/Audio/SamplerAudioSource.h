/*
 ==================================================================================
 
 SamplerAudioSource.h
 Created: 25 Jan 20120 11:34:00am
 Author:  Anantha Girijavallabhan
 
 ==================================================================================
 */
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
//==================================================================================
/**
 
    Class for the instrument sampler synthesiser.
 
    This class is based roughly on the JUCE Tutorial: Build an audio player.
 */
class SamplerAudioSource :   public AudioSource
{
public:
    /** Constructor */
    SamplerAudioSource (MidiKeyboardState& keyState);
    
    /** Constructor */
    ~SamplerAudioSource ();

    /** Loads the base sample for the sampler synthesiser, specifying sampler name and base note.
     
        @param index    an index representing the instrument sample to be selected
     
        @see setUpSampler
    */
    void loadSampleFile (int index);
    
    /** Resamples the sampler synthesiser based on the current sample rate.
     
        @param sr       represents the new sample rate that the synthesiser has to be resampled to
     
        @see setUpSampler
        @see resetSampler
     */
    void setSampleRate (double sr);
    
    /** Sets up the sampler input stream for the synthesiser.
     
        @see setSampleRate
        @see resetSampler
        @see loadSampleFile
     */
    void setUpSampler ();
    
    /** Resets the sampler for updating envelope parameters.
     
        @see setSampleRate
        @see setUpSampler
     */
    void resetSampler ();

    /** Sets the attack and release parameters for the sampler synthesiser.
     
        @param atkTime      the new attack time for the sampler synthesiser's envelope
        @param rlsTime      the new release time for the sampler synthesiser's envelope
     */
    void setEnvelope (float atkTime, float rlsTime);
    
    //Overridden====================================================================
    /* Sets up the sampler synthesiser prior to playback */
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
    /* Releases sampler resources */
    void releaseResources () override;
    /* Responsible for the sampler synthesiser's audio processing */
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override;
    
private:
    //==============================================================================
    MidiKeyboardState& keyboardState;
    BigInteger allNotes;
    std::unique_ptr<AudioFormatReader> audioReader;
    Synthesiser samplerSynth;
    
    float attackTime;
    float releaseTime;
    
    String fileName;
    String samplerName;
    int baseSampleNote;
    
};
