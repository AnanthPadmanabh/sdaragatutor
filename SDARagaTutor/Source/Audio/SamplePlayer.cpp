/*
 ==================================================================================
 
 SamplePlayer.cpp
 Created: 28 Jan 2020 11:56:40pm
 Author:  Anantha Girijavallabhan
 
 ==================================================================================
 */

#include "SamplePlayer.h"

//==============================================================================
SamplePlayer::SamplePlayer ()
{
    formatManager.registerBasicFormats();
    transportSource.addChangeListener(this);
    getTransport().setGain(0.4);
}
//==============================================================================
SamplePlayer::~SamplePlayer ()
{
    formatManager.clearFormats();
    transportSource.releaseResources();
    fileBuffer.clear();
}
//==============================================================================
void SamplePlayer::loadSampleFile (String file)
{
    fileName = file;
}
//==============================================================================
void SamplePlayer::setUpSampler ()
{
    auto parentDir = File::getSpecialLocation(File::SpecialLocationType::currentApplicationFile).getParentDirectory();
    
    while(parentDir.getFileName() != "SDARagaTutor")
    {
        parentDir = parentDir.getParentDirectory();
    }
    
    auto trigFile = parentDir.getChildFile("Samples").getChildFile(fileName);
    
    auto* reader = formatManager.createReaderFor (trigFile);
    
    if (reader != nullptr)
    {
        std::unique_ptr<AudioFormatReaderSource> newSource (new AudioFormatReaderSource (reader, true));
        transportSource.setSource (newSource.get(), 0, nullptr, reader->sampleRate);
        readerSource.reset (newSource.release());
        
        fileBuffer.setSize(reader->numChannels, (int)reader->lengthInSamples);
        reader->read( &fileBuffer, 0, (int)reader->lengthInSamples, 0, true, true);
    }
}
//==============================================================================
void SamplePlayer::setStopping ()
{
    changeState(Stopping);
}
//==============================================================================
void SamplePlayer::setStarting ()
{
    changeState(Starting);
}
//==============================================================================
void SamplePlayer::setGain(float gain)
{
    transportSource.setGain(gain);
}
//==============================================================================
void SamplePlayer::changeListenerCallback (ChangeBroadcaster* source)
{
    if (source == &transportSource)
    {
        if (isPlaying())
        {
            changeState (Playing);
        }
        else
            changeState (Stopped);
    }
    
}
//==============================================================================
