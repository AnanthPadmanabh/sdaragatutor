/*
 ==================================================================================
 
 DroneSamplePlayer.cpp
 Created: 28 Jan 2020 11:56:00pm
 Author:  Anantha Girijavallabhan
 
 ==================================================================================
 */

#include "DroneSamplePlayer.h"

//==============================================================================
DroneSamplePlayer::DroneSamplePlayer()
{
    loadSampleFile("CDrone.wav");  // Loads initial Drone sample in the key of C
}
//==============================================================================
DroneSamplePlayer::~DroneSamplePlayer()
{
    //===========================
}
//==============================================================================
