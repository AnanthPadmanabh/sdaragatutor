/*
 ===============================================================================
 
 SamplerAudioSource.cpp
 Created: 25 Jan 20120 11:34:00am
 Author:  Anantha Girijavallabhan
 
 ===============================================================================
 */

#include "SamplerAudioSource.h"

//==============================================================================
SamplerAudioSource::SamplerAudioSource (MidiKeyboardState& keyState)
                                           : keyboardState(keyState)
{
    for (int i = 0; i < 9; i++)
    {
        samplerSynth.addVoice (new SamplerVoice ());
    }
    
    loadSampleFile(0);
}
//==============================================================================
SamplerAudioSource::~SamplerAudioSource() 
{
    samplerSynth.clearSounds();
    samplerSynth.clearVoices();
}
//==============================================================================
void SamplerAudioSource::loadSampleFile (int index)
{
    switch(index)
    {
        case 0 :
            fileName = "Harmonium.aif";
            samplerName = "Harmonium Sampler";
            baseSampleNote = 73;

            break;
            
        case 1:
            fileName = "Fiddle.aif";
            samplerName = "Fiddle Sampler";
            baseSampleNote = 94;
            
            break;
            
        case 2:
            fileName = "Sitar.aif";
            samplerName = "Sitar Sampler";
            baseSampleNote = 86;
            
            break;
    }
    
    setUpSampler();
}
//==============================================================================
void SamplerAudioSource::setSampleRate (double sr)
{
    samplerSynth.setCurrentPlaybackSampleRate(sr);
}
//==============================================================================
void SamplerAudioSource::setUpSampler ()
{
    AiffAudioFormat aifFormat;
    
    auto parentDirectory = File::getSpecialLocation (File::SpecialLocationType::currentApplicationFile).getParentDirectory ();
    
    while(parentDirectory.getFileName () != "SDARagaTutor")
    {
        parentDirectory = parentDirectory.getParentDirectory ();
    }
    
    auto sampleFile = parentDirectory.getChildFile ("Samples").getChildFile ("Sampler").getChildFile (fileName);
    
    audioReader.reset(aifFormat.createReaderFor (sampleFile.createInputStream(), true));
    
    allNotes.setRange (0, 128, true);
    
    resetSampler();
}
//==============================================================================
void SamplerAudioSource::resetSampler ()
{
    samplerSynth.clearSounds();
    samplerSynth.addSound( new SamplerSound (samplerName, *audioReader, allNotes, baseSampleNote, attackTime, releaseTime, 20.0));
}
//==============================================================================
void SamplerAudioSource::setEnvelope (float atkTime, float rlsTime)
{
    attackTime = atkTime;
    releaseTime = rlsTime;
}
//==============================================================================
void SamplerAudioSource::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    samplerSynth.setCurrentPlaybackSampleRate (sampleRate);
}
//==============================================================================
void SamplerAudioSource::releaseResources ()
{
    samplerSynth.removeSound(0);
}
//==============================================================================
void SamplerAudioSource::getNextAudioBlock (const AudioSourceChannelInfo &bufferToFill)
{
    bufferToFill.clearActiveBufferRegion ();
    
    MidiBuffer incomingMidi;
    
    int startSample = bufferToFill.startSample;
    
    int numSamples = bufferToFill.numSamples;
    
    keyboardState.processNextMidiBuffer (incomingMidi, startSample, numSamples, true);
    
    samplerSynth.renderNextBlock (*bufferToFill.buffer, incomingMidi, startSample, numSamples);
}
//==============================================================================
