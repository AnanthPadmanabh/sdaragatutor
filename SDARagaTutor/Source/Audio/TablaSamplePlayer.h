/*
 ==================================================================================
 
 TablaSamplePlayer.h
 Created: 28 Jan 2020 11:57:00pm
 Author:  Anantha Girijavallabhan
 
 ==================================================================================
 */
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

#include "SamplePlayer.h"
//==================================================================================
/**
 
    Class for the tabla sample loop player, simulating rhythm accompaniment.
 
    This class inherits from SamplePlayer base class.
 
    @see SamplePlayer
    @see DroneSamplePlayer
 */
class TablaSamplePlayer :   public SamplePlayer
{
public:
    /** Constructor */
    TablaSamplePlayer();
    /** Destructor */
    ~TablaSamplePlayer();
    //=============================================================================
};

