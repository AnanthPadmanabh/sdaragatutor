/*
 ==================================================================================
 
 DroneSamplePlayer.h
 Created: 28 Jan 2020 11:56:00pm
 Author:  Anantha Girijavallabhan
 
 ==================================================================================
 */
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

#include "SamplePlayer.h"
//==================================================================================
/**
 
    Class for the drone sample player, simulating a shruti box.
 
    @see SamplePlayer
 */
class DroneSamplePlayer :   public SamplePlayer
{
public:
    /** Constructor */
    DroneSamplePlayer();
    /** Destructor */
    ~DroneSamplePlayer();
    //=============================================================================
};
