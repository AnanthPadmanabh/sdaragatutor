/*
  ==============================================================================

    AudioAndMIDI.cpp
    Created: 3 Dec 2019 9:14:00pm
    Author:  Anantha Girijavallabhan

  ==============================================================================
*/

#include "AudioAndMIDI.h"

//==============================================================================
AudioAndMIDI::AudioAndMIDI () : samplerAudioSource(keyState),
                                reverbSource (&samplerAudioSource, false)
{
    setAudioChannels(0, 2);
    deviceManager.addMidiInputCallback(String(), this);
    
    audioMixer.addInputSource(&reverbSource, false);
    audioMixer.addInputSource(&droneSample.getTransport(), false);
    audioMixer.addInputSource(&tablaSample.getTransport(), false);
}
//==============================================================================
AudioAndMIDI::~AudioAndMIDI()
{
    deviceManager.removeMidiInputCallback(String(), this);
    
    audioMixer.removeAllInputs();
    shutdownAudio();
}
//==============================================================================
void AudioAndMIDI::setSamplerInstrument(int instrumentNum)
{
    samplerAudioSource.loadSampleFile(instrumentNum);
}
//==============================================================================
void AudioAndMIDI::setSamplerEnvelope(float attackTime, float releaseTime)
{
    samplerAudioSource.setEnvelope(attackTime, releaseTime);
    samplerAudioSource.resetSampler();
}
//==============================================================================
void AudioAndMIDI::setReverbLevels(float wetLevel)
{
    customParameters.wetLevel = wetLevel;
    customParameters.dryLevel = 1 - wetLevel;
    reverbSource.setParameters(customParameters);
}
//==============================================================================
void AudioAndMIDI::setReverbSize(float roomSize)
{
    customParameters.roomSize = roomSize;
    reverbSource.setParameters(customParameters);
}
//==============================================================================
void AudioAndMIDI::setDroneKey(String keyName)
{
    droneSample.loadSampleFile(keyName + "Drone.wav");
    droneSample.setUpSampler();
    droneSample.setStarting();
    tablaSample.setStarting();
}
//==============================================================================
void AudioAndMIDI::setDroneLevel(float droneLevel)
{
    droneSample.setGain(droneLevel);
}
//==============================================================================
void AudioAndMIDI::setTablaLevel(float tablaLevel)
{
    tablaSample.setGain(tablaLevel);
}
//==============================================================================
void AudioAndMIDI::handleIncomingMidiMessage(MidiInput *source , const MidiMessage &message)
{
    int noteNum = message.getNoteNumber();
    int noteVel = message.getFloatVelocity();

    if(message.isNoteOn())
    {
        if(noteVel == 0.0)
        {
            noteVel = 1.0; // Overriding the velocity for note on messages from the Virtual Impulse which outputs 0.0 velocity for notes triggered by mouseclicks
        }
        
        keyState.noteOn(1, noteNum, noteVel);
    }
    
    else if(message.isNoteOff() || noteVel == 0.0)
    {
        keyState.noteOff(1, noteNum, noteVel);
    }

}
//==============================================================================
void AudioAndMIDI::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    audioMixer.prepareToPlay(samplesPerBlockExpected, sampleRate);
    
    reverbSource.setBypassed(false);
    reverbSource.setParameters(customParameters);

    samplerAudioSource.setSampleRate(sampleRate);
    samplerAudioSource.setUpSampler();
    
    droneSample.setUpSampler();
    droneSample.setStarting();
    
    tablaSample.setUpSampler();
    tablaSample.setStarting();
    
}
//==============================================================================
void AudioAndMIDI::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    bufferToFill.clearActiveBufferRegion();
    
    audioMixer.getNextAudioBlock (bufferToFill);
}
//==============================================================================
void AudioAndMIDI::releaseResources()
{
    audioMixer.releaseResources();
}
//==============================================================================
