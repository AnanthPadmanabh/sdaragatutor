/*
  ==================================================================================

    AudioAndMIDI.h
    Created: 3 Dec 2019 9:14:00pm
    Author:  Anantha Girijavallabhan

  ==================================================================================
*/
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

#include "SamplerAudioSource.h"
#include "DroneSamplePlayer.h"
#include "TablaSamplePlayer.h"
//==================================================================================
/**
 
    Class containing all the audio and MIDI processes for the application.
 
    Contains the sampler synthesiser as well as the drone and tabla sample players.
 
    @see SamplerAudioSource
    @see DroneSamplePlayer
    @see TablaSamplePlayer
 */
class AudioAndMIDI :    public AudioAppComponent,
                        public MidiInputCallback
{
public:
    /** Constructor */
    AudioAndMIDI ();
    
    /** Destructor */
    ~AudioAndMIDI ();
    //==============================================================================
   
    /** Sets the instrument sample for the sampler synthesiser.
     
        @param instrumentNum     the index representing the instrument to be sampled
        @see setSamplerEnvelope
    */
    void setSamplerInstrument (int instrumentNum);
    
    /** Sets the attack and release parameters of the sampler synthesiser's envelope.
     
        @param attackTime        the attack time of the sampler synthesiser's envelope
        @param releaseTime       the release time of the sampler synthesiser's envelope
        @see setSamplerInstrument
    */
    void setSamplerEnvelope (float attackTime, float releaseTime);
    
    /** Sets the wet level for the reverb effect applied to the sampler synthesiser.
     
        @param wetLevel          the wet level for the reverb effect
        @see setReverbSize
    */
    void setReverbLevels (float wetLevel);
    
    /** Sets the room size for the reverb effect applied to the sampler synthesiser.
     
        @param roomSize          the room size for the reverb effect
        @see setReverbLevels
     */
    void setReverbSize (float roomSize);
    
    /** Sets the appropriate drone sample based on the current selected key.
     
        @param keyName           the newly selected musical key
        @see setDroneLevel
        @see setTablaLevel
     */
    
    void setDroneKey (String keyName);
    
    /** Sets the level of the drone instrument sampler.
     
        @param droneLevel        the drone sampler's new level
        @see setDroneKey
     */
    void setDroneLevel (float droneLevel);
    
    /** Sets the level of the tabla sample loop.
     
        @param tablaLevel        the tabla sample's new level
        @see setDroneLevel
     */
    void setTablaLevel (float tablaLevel);
    
    /** Returns the keyboard state of the sampler synthesiser component.
     
        @return keyState         the sampler synthesiser component's keyboard state
     */
    MidiKeyboardState& getMidiKeyboardState() { return keyState; }
    
    //Overridden====================================================================
    /** Receives incoming messages and feeds them to the sampler synthesiser.
     */
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    /** Prepares the sampler, reverb and synthesiser audio sources prior to playback.
     */
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
    /** Processes sample blocks.
     */
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override;
    /** Releases the audio resources post playback.
     */
    void releaseResources () override;

private:
    //==============================================================================
    MidiKeyboardState keyState;
    
    SamplerAudioSource samplerAudioSource;
    ReverbAudioSource reverbSource;
    MixerAudioSource audioMixer;
    
    DroneSamplePlayer droneSample;
    TablaSamplePlayer tablaSample;
    
    Reverb::Parameters customParameters;
    
};
