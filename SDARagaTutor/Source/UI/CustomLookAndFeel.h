/*
  ==============================================================================

    CustomLookAndFeel.h
    Created: 3 Dec 2019 8:56:39pm
    Author:  Anantha Girijavallabhan

  ==============================================================================
*/
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
//==================================================================================
/**
 
    Class containing custom UI for the application.
 
    This class is based roughly on the JUCE Tutorial: Customise the look and feel of
    your app.
 */
class CustomLookAndFeel : public LookAndFeel_V4
{
public:
    /** Constructor */
    CustomLookAndFeel ();
    
    /** Destructor */
    ~CustomLookAndFeel ();
    
    //Overriden==================================================================
    /** Draws a custom rotary slider */
    void drawRotarySlider (Graphics &g, int x, int y, int width, int height, float sliderPosProportional, float rotaryStartAngle, float rotaryEndAngle, Slider &slider) override;
    //===========================================================================
};
