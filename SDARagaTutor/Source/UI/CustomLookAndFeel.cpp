/*
  ==============================================================================

    CustomLookAndFeel.cpp
    Created: 3 Dec 2019 8:56:39pm
    Author:  Anantha Girijavallabhan

  ==============================================================================
*/

#include "CustomLookAndFeel.h"

CustomLookAndFeel::CustomLookAndFeel()
{
    // Rotary Slider
    setColour(Slider::textBoxOutlineColourId, Colours::sandybrown);
    setColour(Slider::textBoxTextColourId, Colours::black);
    setColour(Slider::textBoxBackgroundColourId, Colours::saddlebrown);
    
    // Combo Box
    setColour(ComboBox::backgroundColourId, Colours::saddlebrown);
    setColour(ComboBox::arrowColourId, Colours::black);
    setColour(ComboBox::outlineColourId, Colours::black);
    setColour(ComboBox::textColourId, Colours::black);
    setColour(ComboBox::textColourId, Colours::black);
    
    // Text Button
    setColour(TextButton::buttonOnColourId, Colours::saddlebrown);
    setColour(TextButton::buttonColourId, Colours::sandybrown);
    setColour(TextButton::textColourOnId, Colours::white);
    setColour(TextButton::textColourOffId, Colours::black);
    
    // Keyboard Component
    setColour(MidiKeyboardComponent::mouseOverKeyOverlayColourId, Colours::lawngreen);
    setColour(MidiKeyboardComponent::keyDownOverlayColourId, Colours::lawngreen);
    setColour(MidiKeyboardComponent::upDownButtonArrowColourId, Colours::greenyellow);
    
    // Label
    setColour(Label::textColourId, Colours::yellow);
}
//==============================================================================
CustomLookAndFeel::~CustomLookAndFeel()
{
    //==========================
}
//==============================================================================
void CustomLookAndFeel::drawRotarySlider(Graphics &g, int x, int y, int width, int height, float sliderPosProportional, float rotaryStartAngle, float rotaryEndAngle, Slider &slider)
{
    float diameter = jmin(width/2, height/2) - 2.0f;
    float radius = diameter/2;
    float centreX = x + width/2;
    float centreY = y + height/2;
    float rX = centreX - radius;
    float rY = centreY - radius;
    float tickAngle = rotaryStartAngle + sliderPosProportional * (rotaryEndAngle - rotaryStartAngle);
    
    Rectangle<float> dialArea(rX, rY, diameter, diameter);
    
    g.setColour(Colours::saddlebrown);
    g.fillRoundedRectangle(dialArea, 25.0);
    g.setColour(Colours::white);
    
    Path dialTick;
    dialTick.addRectangle(0, -radius, 2.5F, radius/2);
    g.setColour(Colours::black );
    g.fillPath(dialTick, AffineTransform::rotation(tickAngle).translated(centreX, centreY));
    
    // Dial outline
    g.setColour(Colours::black);
    g.drawRoundedRectangle(dialArea, 25.0, 1.0);
    
}
//==============================================================================
