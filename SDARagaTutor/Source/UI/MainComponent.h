/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/
#pragma once

#include <string>

#include "../JuceLibraryCode/JuceHeader.h"
#include "../Audio/AudioAndMIDI.h"
#include "CustomLookAndFeel.h"
//==============================================================================
/** Class containing all the GUI for the application. */

class MainComponent :    public Component,
                         public MenuBarModel,
                         public Timer,
                         public Slider::Listener
{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (AudioAndMIDI& am);
    /** Destructor */
    ~MainComponent ();
    //Overriden menu functions======================================================
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected(int menuID, int index) override;
    //Overriden component functions=================================================
    void paint (Graphics& g) override;
    void resized() override;
    bool keyPressed(const KeyPress& key) override;
    void mouseDown(const MouseEvent &event) override;
    //Slider callback===============================================================
    void sliderValueChanged (Slider* slider) override;
    //MenuEnums=====================================================================
    enum MenuIDs
    {
        FileMenu = 1000,
        OuterMenu = 1001
    };

    enum FileMenuItemsID
    {
        AudioSettings = 1000
    };
    //Dropdown Callbacks============================================================
    void keyChanged();
    void ragaChanged();
    void sampleChanged();
private:
    //==============================================================================
    
    void timerCallback() override;
    
    // UI
    Label appName;
    Label keyLabel;
    Label ragaLabel;
    Label attackLabel;
    Label releaseLabel;
    Label droneLvlLabel;
    Label reverbLvlLabel;
    Label tablaLvlLabel;
    Label reverbSzeLabel;
    
    ComboBox keySelector;
    ComboBox ragaSelector;
    ComboBox sampleSelector;
    
    Slider attackDial;
    Slider releaseDial;
    Slider droneLvlDial;
    Slider tablaLvlDial;
    Slider reverbLvlDial;
    Slider roomSzeDial;
    
    CustomLookAndFeel customLookAndFeel;
    
    // Audio
    AudioAndMIDI& audioAndMIDI;
    
    MidiKeyboardState visualKeyboardState;
    
    // Keyboard
    MidiKeyboardComponent soundingKeyboardComponent;
    MidiKeyboardComponent visualKeyboardComponent;
    
    int transposeBySemitones;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};

