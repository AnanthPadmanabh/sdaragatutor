/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent (AudioAndMIDI& am)   : audioAndMIDI(am)
                                                   ,soundingKeyboardComponent(audioAndMIDI.getMidiKeyboardState()
                                                   ,MidiKeyboardComponent::horizontalKeyboard)
                                                   ,visualKeyboardComponent(visualKeyboardState, MidiKeyboardComponent::horizontalKeyboard)
{
    setMacMainMenu( this );
    setSize (800, 600);
    startTimer(500);
    
    addAndMakeVisible(&appName);
    addAndMakeVisible(&attackDial);
    addAndMakeVisible(&attackLabel);
    addAndMakeVisible(&releaseLabel);
    addAndMakeVisible(&releaseDial);
    addAndMakeVisible(&soundingKeyboardComponent);
    addAndMakeVisible(&visualKeyboardComponent);
    addAndMakeVisible(&keySelector);
    addAndMakeVisible(&keyLabel);
    addAndMakeVisible(&droneLvlDial);
    addAndMakeVisible(&droneLvlLabel);
    addAndMakeVisible(&tablaLvlDial);
    addAndMakeVisible(&tablaLvlLabel);
    addAndMakeVisible(&reverbLvlDial);
    addAndMakeVisible(&reverbLvlLabel);
    addAndMakeVisible(&roomSzeDial);
    addAndMakeVisible(&reverbSzeLabel);
    addAndMakeVisible(&ragaSelector);
    addAndMakeVisible(&ragaLabel);
    addAndMakeVisible(&sampleSelector);
    
    appName.setText("SDARAGATUTOR", NotificationType::sendNotification);
    appName.setColour(Label::textColourId, Colours::yellow);
    appName.setWantsKeyboardFocus(false);
    
    attackDial.setSliderStyle(Slider::Rotary);
    attackDial.setTextBoxStyle(Slider::TextBoxBelow, false, getWidth()/25, getHeight()/25);
    attackDial.setRange(Range<double>(0.0,1.0),0.1);
    attackDial.setDoubleClickReturnValue(true, 0.5);
    attackDial.setWantsKeyboardFocus(false);
    
    releaseDial.setSliderStyle(Slider::Rotary);
    releaseDial.setTextBoxStyle(Slider::TextBoxBelow, false, getWidth()/25, getHeight()/25);
    releaseDial.setRange(Range<double>(0.0,1.0),0.1);
    releaseDial.setDoubleClickReturnValue(true, 0.5);
    releaseDial.setWantsKeyboardFocus(false);
    
    droneLvlDial.setSliderStyle(Slider::Rotary);
    droneLvlDial.setTextBoxStyle(Slider::TextBoxBelow, false, getWidth()/25, getHeight()/25);
    droneLvlDial.setRange(Range<double>(0.0,1.0),0.1);
    droneLvlDial.setTextBoxIsEditable(true);
    droneLvlDial.setDoubleClickReturnValue(true, 0.5);
    droneLvlDial.setWantsKeyboardFocus(false);
    
    tablaLvlDial.setSliderStyle(Slider::Rotary);
    tablaLvlDial.setTextBoxStyle(Slider::TextBoxBelow, false, getWidth()/25, getHeight()/25);
    tablaLvlDial.setRange(Range<double>(0.0,1.0),0.1);
    tablaLvlDial.setDoubleClickReturnValue(true, 0.5);
    tablaLvlDial.setWantsKeyboardFocus(false);
    
    reverbLvlDial.setSliderStyle(Slider::Rotary);
    reverbLvlDial.setTextBoxStyle(Slider::TextBoxBelow, false, getWidth()/25, getHeight()/25);
    reverbLvlDial.setRange(Range<double>(0.0,1.0),0.1);
    reverbLvlDial.setDoubleClickReturnValue(true, 0.5);
    reverbLvlDial.setWantsKeyboardFocus(false);
    
    roomSzeDial.setSliderStyle(Slider::Rotary);
    roomSzeDial.setTextBoxStyle(Slider::TextBoxBelow, false, getWidth()/25, getHeight()/25);
    roomSzeDial.setRange(Range<double>(0.0,1.0),0.1);
    roomSzeDial.setDoubleClickReturnValue(true, 0.5);
    roomSzeDial.setWantsKeyboardFocus(false);
    
    keyLabel.setText("Key", dontSendNotification);
    ragaLabel.setText("Raga", dontSendNotification);
    attackLabel.setText("Attack", dontSendNotification);
    releaseLabel.setText("Release", dontSendNotification);
    droneLvlLabel.setText("Drone", dontSendNotification);
    tablaLvlLabel.setText("Tabla", dontSendNotification);
    reverbLvlLabel.setText("Reverb", dontSendNotification);
    reverbSzeLabel.setText("Size", dontSendNotification);
    
    tablaLvlLabel.toBack();
    
    attackDial.setValue(0.2);
    releaseDial.setValue(0.3);
    droneLvlDial.setValue(0.4);
    tablaLvlDial.setValue(0.4);
    reverbLvlDial.setValue(0.2);
    roomSzeDial.setValue(0.3);
    
    soundingKeyboardComponent.setAvailableRange(72, 95);
    soundingKeyboardComponent.setWantsKeyboardFocus(true);
    soundingKeyboardComponent.setLookAndFeel(&customLookAndFeel);
    
    visualKeyboardComponent.setAvailableRange(60, 71);
    visualKeyboardComponent.setWantsKeyboardFocus(false);
    visualKeyboardComponent.setColour(MidiKeyboardComponent::keyDownOverlayColourId, Colours::royalblue);
    
    visualKeyboardComponent.addMouseListener(this, false);

    transposeBySemitones = 0;
    
    keySelector.addItemList({"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"}, 1);
    keySelector.setTextWhenNothingSelected("C");
    keySelector.setJustificationType(Justification::centred);
    keySelector.setWantsKeyboardFocus(false);

    ragaSelector.addItemList({"Bhairav", "Bhairavi", "Yaman"}, 1);
    ragaSelector.setSelectedId(1);
    ragaSelector.setJustificationType(Justification::centred);
    ragaSelector.setWantsKeyboardFocus(false);
    
    sampleSelector.addItemList({"Harmonium", "Fiddle", "Sitar"}, 1);
    sampleSelector.setSelectedId(1);
    sampleSelector.setJustificationType(Justification::centred);
    sampleSelector.setWantsKeyboardFocus(false);
    
    setLookAndFeel(&customLookAndFeel);
    
    attackDial.addListener(this);
    releaseDial.addListener(this);
    droneLvlDial.addListener(this);
    tablaLvlDial.addListener(this);
    reverbLvlDial.addListener(this);
    roomSzeDial.addListener(this);
    
    keySelector.onChange = [this] { keyChanged(); };
    ragaSelector.onChange = [this] { ragaChanged(); };
    sampleSelector.onChange = [this] { sampleChanged(); };
    
}
//==============================================================================
MainComponent::~MainComponent()
{
    setLookAndFeel(nullptr);
    setMacMainMenu(nullptr);
}
//==============================================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = {"File", 0};
    return StringArray (names);
}
//==============================================================================
PopupMenu MainComponent::getMenuForIndex(int topLevelMenuIndex, const String &menuMame)
{
    PopupMenu menu;
    if(topLevelMenuIndex == 0)
    {
        menu.addItem(AudioSettings, String (CharPointer_UTF8 ("Audio MIDI Settings   ⌘ + ,")), true, false);
    }
    
    return menu;
}
//==============================================================================
void MainComponent::menuItemSelected(int menuID, int index)
{
    if(menuID == FileMenu)
    {
        if(menuID == AudioSettings)
        {
            AudioDeviceSelectorComponent audioMIDISettingsComponent(audioAndMIDI.deviceManager, 0, 0, 2, 2, true, false, true, false);
            
            audioMIDISettingsComponent.setSize(400, 400);
            DialogWindow::showModalDialog("Audio MIDI Settings", &audioMIDISettingsComponent, this, Colours::black, true);
        }
    }
}
//==============================================================================
void MainComponent::paint (Graphics& g)
{
    auto baseHue = Colours::saddlebrown.getHue();
    Colour colours = Colour::fromHSV(baseHue, 0.3, 0.5, 0.9);
    
    g.setColour(colours);
    g.fillAll();
}
//==============================================================================
void MainComponent::resized()
{
    appName.setBounds(0, 0, getWidth(), getHeight() * 0.2);
    appName.setJustificationType(Justification::centred);
    
    soundingKeyboardComponent.setBounds(getWidth()/16, getHeight() * 0.8, getWidth() * 0.875, getHeight() * 0.2);
    
    soundingKeyboardComponent.setKeyWidth(getWidth()/15.9);
    
    visualKeyboardComponent.setBounds(getWidth()/4, getHeight()/4, getWidth()/2, getHeight()/4);

    visualKeyboardComponent.setKeyWidth(getWidth()/16.0);
    
    attackDial.setBounds(getWidth() * 0.22, getHeight() * 0.55, getWidth()/4, getHeight()/5); // * getWidth() * 0.78
    
    attackLabel.setBounds(getWidth() * 0.255, getHeight() * 0.63, getWidth()/4, getHeight()/5);
    
    releaseDial.setBounds(getWidth() * 0.525, getHeight() * 0.55, getWidth()/4, getHeight()/5);
    
    releaseLabel.setBounds(getWidth() * 0.679, getHeight() * 0.63, getWidth()/4, getHeight()/5);
    
    droneLvlDial.setBounds(getWidth() * 0.01, getHeight() * 0.55, getWidth()/4, getHeight()/5);
    
    droneLvlLabel.setBounds(getWidth() * 0.05, getHeight() * 0.63, getWidth()/4, getHeight()/5);
    
    tablaLvlDial.setBounds(getWidth() * 0.01, getHeight() * 0.35, getWidth()/4, getHeight()/5);
    
    tablaLvlLabel.setBounds(getWidth() * 0.05, getHeight() * 0.43, getWidth()/4, getHeight()/5);
    
    reverbLvlDial.setBounds(getWidth() * 0.74, getHeight() * 0.55, getWidth()/4, getHeight()/5);
    
    reverbLvlLabel.setBounds(getWidth() * 0.89, getHeight() * 0.63, getWidth()/4, getHeight()/5);
    
    roomSzeDial.setBounds(getWidth() * 0.74, getHeight() * 0.35, getWidth()/4, getHeight()/5);
    
    reverbSzeLabel.setBounds(getWidth() * 0.89, getHeight() * 0.43, getWidth()/4, getHeight()/5);
    
    keySelector.setBounds(getWidth() * 0.054, getHeight()/4, getWidth()/8, getHeight()/16);
    
    keyLabel.setBounds(keySelector.getX(), getHeight()/5, keySelector.getWidth(), keySelector.getHeight());
    
    ragaSelector.setBounds(getWidth() * 0.82, getHeight()/4, getWidth()/8, getHeight()/16);
    
    ragaLabel.setBounds(ragaSelector.getX() + ragaSelector.getWidth()/2, getHeight()/5, ragaSelector.getWidth(), ragaSelector.getHeight());
    
    sampleSelector.setBounds(getWidth()/3, getHeight()/2 * 1.02, getWidth()/3, getHeight()/15);
}
//==============================================================================
bool MainComponent::keyPressed (const KeyPress &key)
{
    if(key.getKeyCode() == 44)
    {
    ModifierKeys modifierKey = key.getModifiers();
    
    if(modifierKey.isAnyModifierKeyDown())
    {
        
            menuItemSelected(FileMenu, 1);
    }
    }
    
    return true;
}
//==============================================================================
void MainComponent::mouseDown(const MouseEvent &event)
{
    ragaChanged();
}
//==============================================================================
void MainComponent::sliderValueChanged (Slider* slider)
{
    if (slider == &attackDial || slider == &releaseDial)
    {
        audioAndMIDI.setSamplerEnvelope(attackDial.getValue(), releaseDial.getValue());
    }
    
    float sliderVal = slider->getValue();
    
    if (slider == &reverbLvlDial)
    {
        audioAndMIDI.setReverbLevels(sliderVal);
    }
    
    if (slider == &droneLvlDial)
    {
        audioAndMIDI.setDroneLevel(sliderVal);
    }
    
    if (slider == &tablaLvlDial)
    {
        audioAndMIDI.setTablaLevel(sliderVal);
    }
    
    if (slider == &roomSzeDial)
    {
        audioAndMIDI.setReverbSize(sliderVal);
    }
    
    soundingKeyboardComponent.grabKeyboardFocus(); 
}
//==============================================================================
void MainComponent::keyChanged()
{
    audioAndMIDI.setDroneKey(keySelector.getItemText(keySelector.getSelectedItemIndex()));
    transposeBySemitones = keySelector.getSelectedItemIndex();
    int startNote = 60 + transposeBySemitones;
    visualKeyboardComponent.setAvailableRange(startNote, startNote + 12);
    ragaChanged();
}
//==============================================================================
void MainComponent::ragaChanged()
{
    visualKeyboardState.allNotesOff(1);
    
    enum RagaName
    {
        ragaBhairav = 1,
        ragaBhairavi = 2,
        ragaYaman = 3
    };
    
    int bhairavNoteIntervals[] = { 0, 1, 4, 5, 7, 8, 11, 12 };
    int bhairaviNoteIntervals[] = { 0, 1, 3, 5, 7, 8, 10, 12 };
    int yamanNoteIntervals[] = { 0, 2, 4, 6, 7, 9, 11, 12 };
    
    int *currentRagaNote;
    
    switch (ragaSelector.getSelectedId())
    {
        case ragaBhairav:
            
            currentRagaNote = bhairavNoteIntervals;
            
            break;
            
        case ragaBhairavi:

            
            currentRagaNote = bhairaviNoteIntervals;
            
            break;
            
        case ragaYaman:
            
            currentRagaNote = yamanNoteIntervals;
            
    }
            
        for (int i = 0; i <= 7; i++)
        {
            visualKeyboardState.noteOn(1, 60 + *currentRagaNote + transposeBySemitones, 0.0);
            currentRagaNote++;
        }
    
    }
//==============================================================================
void MainComponent::sampleChanged()
{
    audioAndMIDI.setSamplerInstrument(sampleSelector.getSelectedItemIndex());
}
//==============================================================================
void MainComponent::timerCallback()
{
    soundingKeyboardComponent.grabKeyboardFocus();
    stopTimer();
}
//==============================================================================
